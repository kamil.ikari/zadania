import java.util.Scanner;

public class RownanieKwadratowe {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj a,b,c ");
        int a = Integer.valueOf(scan.nextLine());
        int b = Integer.valueOf(scan.nextLine());
        int c = Integer.valueOf(scan.nextLine());

        double delta = Math.pow(b,2) - (4*a*c);
        System.out.println(delta);
        if (delta < 0){
            System.out.println("Delta Ujemna");
        } else {
            double xJeden = ((-1 * b) - Math.sqrt(delta))/2*a;
            double xDwa = ((-1 * b) + Math.sqrt(delta))/2*a;


            System.out.println("X 1 =" +xJeden);
            System.out.println("X 2 = "+xDwa);
        }


    }
}
