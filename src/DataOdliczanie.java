import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.Scanner;

public class DataOdliczanie {
    public static void main(String[] args) throws ParseException {
        Scanner scan = new Scanner(System.in);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");


        System.out.println("Podaj date (yyyy-mm-dd)");
        String tekst = scan.nextLine();
        LocalDate sda = LocalDate.parse(tekst);
        LocalDate dzis = LocalDate.now();
        Duration difference = Duration.between(dzis.atStartOfDay(), sda.atStartOfDay());


        System.out.println(difference.toDays());


    }
}


