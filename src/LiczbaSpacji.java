import java.util.Scanner;

public class LiczbaSpacji {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String tekst = scanner.nextLine();

        double dlugoscTekstuzeSpacjami = tekst.length();

        String tekstBezSpacji =tekst.replace(" ", "");
        double dlugoscTekstuBezSpacji = tekstBezSpacji.length();

        double liczbaSpacji = dlugoscTekstuzeSpacjami - dlugoscTekstuBezSpacji;

        System.out.println("Spacje to " + ((liczbaSpacji / dlugoscTekstuzeSpacjami) * 100) + " %.");
    }
}
