import java.util.Scanner;

public class LiczbypierwszeB {
    public static void main(String[] args) {

        StringBuilder wynik = new StringBuilder();
        Scanner scan = new Scanner(System.in);
        int zakres = Integer.valueOf(scan.nextLine());

        for (int i = 1; i <= zakres; i++) {
            int licznik = 0;
            for (int num = i; num >= 1; num--) {
                if (i % num == 0) {
                    licznik = licznik + 1;
                }
            }
            if (licznik == 2) {
                wynik.append(i).append(" ");
            }

            System.out.println(wynik.toString());
        }
    }
}
