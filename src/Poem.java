public class Poem {
    private int stropheNumbers;
    private Author creator;

    public Poem (Author creator, int stropheNumbers){
        this.creator = creator;
        this.stropheNumbers = stropheNumbers;

    }

    public int getStropheNumbers() {
        return stropheNumbers;
    }

    public void setStropheNumbers(int StropheNumbers) {
        this.stropheNumbers = StropheNumbers;
    }

    public Author getCreator() {
        return creator;
    }

    public void setCreator(Author creator) {
        this.creator = creator;
    }
}
