import java.util.ArrayList;
import java.util.Scanner;

public class NajdluzszyString {
    public static void main(String[] args) {
        Scanner scan = new Scanner (System.in);
        ArrayList<String> list = new ArrayList<>();

        while (true){
            String input = scan.nextLine();
            if (input.equals("Starczy")){
                break;
            }
            if (input.equals("")){
                System.out.println("Nie podano zadnego tesktu");
                continue;
            }
            list.add(input);
        }

        int liczbaZnakow = 0;
        String najdluzszy = "";
        for (int i = 0; i < list.size(); i++) {
            if (liczbaZnakow < list.get(i).length()) {
                liczbaZnakow = list.get(i).length();
                najdluzszy = list.get(i);
            }
        }
        System.out.println(najdluzszy);

    }
}
