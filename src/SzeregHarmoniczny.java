import java.util.Scanner;

public class SzeregHarmoniczny {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Podaj liczbe");
        int liczba = Integer.valueOf(scan.nextLine());

        double suma = 1.0;

        for (int i = 2; i <= liczba; i++) {
            suma = suma + (1.0/i);
        }

        System.out.println(suma);
    }
}
