import java.util.Scanner;

public class Wspak {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Wprowadz tekst: ");
        String tekst = scan.nextLine();
        String wspak = "";

        for (int i = tekst.length() - 1; i >= 0; i--) {

            wspak = wspak + tekst.charAt(i);
        }

        System.out.println("Reversed string is:");
        System.out.println(wspak);
    }
}

