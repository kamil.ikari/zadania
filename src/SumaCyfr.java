import java.util.Scanner;

public class SumaCyfr {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int liczba = Integer.valueOf(scan.nextLine());
        int suma = 0;
        int modulo = 0;


        while (true) {
            modulo = liczba % 10;
//            System.out.println(modulo);
            suma += modulo;
//            System.out.println(suma);
            liczba /= 10;
//            System.out.println(liczba);
            if (liczba == 0) {
                break;
            }
        }
        System.out.println(suma);

        System.out.println();
        System.out.println("********");
        System.out.println();


        String liczbanaString = String.valueOf(liczba);
        int sumab = 0;
        int ileCyfr = liczbanaString.length();
        for (int i = 0; i<ileCyfr; i++)
        {
            char znak = liczbanaString.charAt(i);
            int cyfra = Character.getNumericValue(znak);
            sumab = sumab + cyfra;
        }
        System.out.println(suma);
    }
}
