import java.util.Scanner;

public class IleZnakowJestPomiedzyLiterami {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String pierwsza = scanner.nextLine();
        String druga = scanner.nextLine();

        char ch1 = pierwsza.charAt(0);
        char ch2 = druga.charAt(0);

        System.out.println((Math.abs(ch2 - ch1))-1);

    }
}
