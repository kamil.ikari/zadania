import java.util.Scanner;

public class BMI {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Podaj wage:");
        float waga = Float.valueOf(scan.nextLine());
        System.out.println("Podaj wzrost ");
        int wzrost = Integer.valueOf(scan.nextLine());
        double wzrostWm = wzrost * 0.01;

        double bmi = (waga / Math.pow(wzrostWm,2));
        System.out.println(bmi);

        if (bmi >= 18.5 && bmi <= 24.9){
            System.out.println("BMI optymalne");
        } else {
            System.out.println("BMI nieoptymalne");
        }

    }
}
