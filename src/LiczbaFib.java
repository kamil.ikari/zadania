import java.util.Scanner;

public class LiczbaFib {
    public static void main(String[] args) {
        long a = 1;
        long b = 1;
        long temp;

        System.out.print("Którą liczbę Fibonacciego wyznaczyć? ");
        Scanner in = new Scanner(System.in);
        long n = Long.valueOf(in.nextLine());

        for (int i = 1; i < n; ++i) {
            temp = a;
            a = b;
            b = a + temp;
        }

        System.out.println(n + " liczba Fibonacciego to " + a);

    }
}

