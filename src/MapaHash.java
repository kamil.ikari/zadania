import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MapaHash {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Map<Integer, Integer> mojeLiczby = new HashMap<>();

        for (int i = 0; i < 5; i++) {
            Integer input = Integer.valueOf(scan.nextLine());
            if (mojeLiczby.containsKey(input)) {
                Integer ileRazyWystapilo = mojeLiczby.get(input); //int wczytuje liczbe, ktora wpisalismy
                mojeLiczby.put(input, ileRazyWystapilo +1); // dodaje do wartosci 1 (hash mapa ma klucz,wartosc)
            } else {
                mojeLiczby.put(input, 1);
            }
         }
        System.out.println(mojeLiczby);

            for (Integer klucz:mojeLiczby.keySet()){
                if(mojeLiczby.get(klucz)>1){
                    System.out.print(klucz + " ");
                }
            }
        }
    }

