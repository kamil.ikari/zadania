public class Main {
    public static void main(String[] args) {

        Author pierwszy = new Author("Mickiewicz","Polak");
        Author drugi = new Author("Sheakspear", "Anglik");
        Author trzeci = new Author("Slowacki","Polak");

        Poem[] poems = new Poem[3];
        poems[0]=new Poem(pierwszy,21);
        poems[1]=new Poem(drugi,33);
        poems[2]=new Poem(trzeci, 44);

        int najdluzszy = poems[0].getStropheNumbers();
        String autorNajdluzszego = poems[0].getCreator().getSurname();

        for (int i = 0; i < poems.length; i++) {
            if(poems[i].getStropheNumbers()>najdluzszy){
                najdluzszy = poems[i].getStropheNumbers();
                autorNajdluzszego = poems[i].getCreator().getSurname();
            }
        }
        System.out.println(autorNajdluzszego);

    }

}
