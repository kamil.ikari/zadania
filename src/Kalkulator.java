import java.util.Scanner;

public class Kalkulator {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Podaj pierwsza liczbe x.y ");
        float a = Float.valueOf(scan.nextLine());
        System.out.println("+ - / * ");
        String dzialanie = scan.nextLine();

        while (true) {
            System.out.println("Podaj druga liczbe x.y");
            float b = Float.valueOf(scan.nextLine());

            if (dzialanie.equals("+")) {
                System.out.println(a + b);
                break;
            }
            if (dzialanie.equals("-")) {
                System.out.println(a - b);
                break;
            }
            if (dzialanie.equals("*")) {
                System.out.println(a * b);
                break;
            }
            if (dzialanie.equals("/")) {
                System.out.println(a / b);
                break;
            } else {
                System.out.println("Blad");
                break;
            }

        }

    }
}

