import java.util.Scanner;

public class Gwiazdki {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        int height = 4;
        int patternLenght = 8;
        System.out.println("Podaj liczbę iteracji:");
        int a = scanner.nextInt();
        int counter = 0;
        for (int i = 0; i < height; i++) {
            counter = 0;
            for (int j = 0; j <= a; j++) {
                if (counter < patternLenght) {
                    if (counter == i || counter == (patternLenght - 1) - i) System.out.print("*");
                    else System.out.print(" ");
                    counter++;
                } else counter = 0;
            }
            System.out.print("\n");
        }
    }
}
